import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  Alert,
  FlatList,
  SectionList,
  ListItem,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import FlatEarthsOCIATY from './FlatEarthsOCIATY';
import MapView from 'react-native-maps';
import articles from './articles.json';
import { StackActions, NavigationActions } from 'react-navigation';
import About from './About';
import createDrawerNavigator from 'react-navigation';
import StackNavigator from 'react-navigation';
import LizardHack from './LizardHack';
import { DrawerNavigator } from 'react-navigation';
import Welcome from './Welcome';


class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={articles.articles}
          renderItem={({ item }) => (
            
            <FlatEarthsOCIATY
              style={styles.item}
              item={item}
              onPress={() =>
                this.props.navigation.navigate('Details', {
                  article: item,
                })
              }
            />
          )}
          keyExtractor={(item, index) => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

export default DrawerNavigator(
  {
    Lizards: {
      screen: App,
    },
    About: {
      screen: About,
    },
    Welcome: {
      screen: Welcome,
    },
    Details: LizardHack,
  },

  {
    initialRouteName: 'Welcome',
  }
)