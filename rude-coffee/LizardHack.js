import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Image, FlatList, ScrollView, Dimensions, } from 'react-native';


export default class LizardHack extends React.Component {
 constructor(props) {
    super(props);
  }

  render() {
    const { navigation } = this.props;
    const article = navigation.getParam('article', {});

    return (
      <ScrollView contentContainerStyle={styles.detailsContainer}>
        <Image source={{ uri: article.image }} style={styles.image}></Image>
        <Text>{article.title}</Text>
        {article.posts.map((post, index) => {
          return (
            <Text key={index}>{post}</Text>
          )
        })}

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  detailsContainer: {
    flexGrow: 1,
    flexBasis: "100%",
    justifyContent: "flex-start",
    backgroundImage: 'https://mir-s3-cdn-cf.behance.net/project_modules/disp/21f0771920358.56010e1ecfc2c.jpg',
    flexDirection: "column",
  },
    
  image: {
    flex: 1,
    width: 350,
    height: 350,
    resizeMode: "contain",
    alignContent: "center",
    
  },
});