import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity, ImageBackground, Font} from 'react-native';
import LabelgOVERmEaNt from './LabelgOVERmEaNt';

export default class FlatEarthsOCIATY extends React.Component {
  constructor(props) {
    super(props);
    this.state = { numColumns: 0 };
  }

  render() {
    
    return (
      <TouchableOpacity onPress={this.props.onPress}>
      
        <View style={{ flex: 1, flexDirection: "column" }}>
        
          <View style={{flex: 1,
                      flexDirection: "row",
                      backgroundColor: "#000000",
                      alignItems: "center"}}>
           <ImageBackground source={{ uri: this.props.item.image }} style={{ width: '100%', height: '140%'}}>
            <View style={{ flex: 1, flexDirection: "column", margin: 20 }}>
             <Text style={styles.Title}>{this.props.item.title}</Text>
              <View style={styles.badgeList}>

                

              </View>
            </View>
             </ImageBackground>
          </View>
          
          <View style={{ height: 2,}}></View>
        
        </View>
        
      </TouchableOpacity>

      
    );
  }
}

const styles = StyleSheet.create({
  badgeList: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-end",
    color:"white"
  },
  Title: {
    marginBottom: 10,
    fontWeight: "bold",
    fontSize: 14,
    color: "white",
    textAlign:"center",
  },
});